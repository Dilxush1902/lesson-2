// Convert the switch statement into an object called lookup.
// Use it to look up val and assign the associated string to the result variable.

// Setup
function phoneticLookup(val) {
  let search = {
    alpha:"Adams",
    charlie:"Chicago",
    delta:"Denver",
    echo:"Easy",
    foxtrot:"Frank",
    bravo:"Boston"
  }
  return search[val]
  // let result = '';
  // Only change code below this line
  // switch (val) {
  //   case 'alpha':
  //     result = 'Adams';
  //     break;
  //   case 'bravo':
  //     result = 'Boston';
  //     break;
  //   case 'charlie':
  //     result = 'Chicago';
  //     break;
  //   case 'delta':
  //     result = 'Denver';
  //     break;
  //   case 'echo':
  //     result = 'Easy';
  //     break;
  //   case 'foxtrot':
  //     result = 'Frank';
  // }

  // Only change code above this line
}

console.log(phoneticLookup('charlie'));  // Chicago
console.log(phoneticLookup('alpha')); // Adams
console.log(phoneticLookup('bravo')); // Boston
console.log(phoneticLookup('') );// undefined
