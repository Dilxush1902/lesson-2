// Write two ways of creating objects using functions that represent
// the items of the products on the products.jpg on the source directory

// Factory Function
let product =(name,type)=> {
    return { name,type}
};

console.log(product('макси бокс','популярный'));
console.log(product('макси бокс',"Petro"));

// Constructor Function
// You code here ...

function Product(name,type) {
    this.name=name;
    this.type=type;
}
const card1= new Product('макси бокс','популярный');
const card2= new Product('макси бокс',"Petro");
const car3= new Product('макси бокс',"традиция");
console.log(card1);
console.log(card2);
